#include "BTagTrackWriter.hh"
#include "BTagTrackWriterConfig.hh"
#include "HDF5Utils/Writer.h"

// Less Standard Libraries (for atlas)
#include "H5Cpp.h"

// ATLAS things
#include "xAODTracking/TrackParticle.h"
#include "xAODJet/Jet.h"

#include "FlavorTagDiscriminants/customGetter.h"
#include "FlavorTagDiscriminants/BTagTrackIpAccessor.h"

/////////////////////////////////////////////
// Internal classes
/////////////////////////////////////////////

typedef std::function<float(const TrackOutputs&)> FloatFiller;
class TrackConsumers: public H5Utils::Consumers<const TrackOutputs&> {};
class TrackOutputWriter: public H5Utils::Writer<1,const TrackOutputs&>
{
public:
  TrackOutputWriter(H5::Group& file,
                    const std::string& name,
                    const TrackConsumers& cons,
                    size_t size):
    H5Utils::Writer<1,const TrackOutputs&>(file, name, cons, {{size}}) {}
};

// wrapper class to convert the things we have in
// FlavorTagDiscriminants into something that HDF5Utils can read.
template <typename T>
class CustomSeqWrapper
{
  using SeqGetter = decltype(
    FlavorTagDiscriminants::customSequenceGetterWithDeps(
      std::declval<std::string>(),
      std::declval<std::string>()
      )
    );
public:
  CustomSeqWrapper(SeqGetter g): m_getter(g.first) {
  }
  T operator()(const TrackOutputs& t) {
    auto output_vec = m_getter(*t.jet, {t.track});
    return output_vec.at(0);
  }
private:
  SeqGetter::first_type m_getter;
};


///////////////////////////////////
// Custom fillers
//////////////////////////////////

std::function<float(const TrackOutputs&)> get_custom(
  const std::string& name,
  const std::string& prefix)
{
  if (name == "dr") {
    return [](const TrackOutputs& trk) -> float {
      return trk.track->p4().DeltaR(trk.jet->p4());
    };
  } else if (name == "ptfrac") {
    return [](const TrackOutputs& trk) -> float {
      return trk.track->pt() / trk.jet->pt();
    };
  } else if (name == "z0RelativeToBeamspotUncertainty") {
    return [](const TrackOutputs& t) -> float {
      return std::sqrt(t.track->definingParametersCovMatrixDiagVec().at(1));
    };
  } else {
    // most of the custom getters can be grabbed from Athena
    auto getter = FlavorTagDiscriminants::customSequenceGetterWithDeps(
      name, prefix);
    // note that within FlavorTagDiscriminants all the floats are
    // double we're truncating here because that precision is
    // probably not needed
    CustomSeqWrapper<float> wrapped(getter);
    return wrapped;
  }
}

///////////////////////////////////
// Class definition starts here
///////////////////////////////////
BTagTrackWriter::BTagTrackWriter(
  H5::Group& output_file,
  const BTagTrackWriterConfig& config):
  m_hdf5_track_writer(nullptr)
{
  H5Utils::Compression standard = H5Utils::Compression::STANDARD;
  H5Utils::Compression half = H5Utils::Compression::HALF_PRECISION;
  const auto& vars = config.variables;
  TrackConsumers fillers;
  // add the variables passed as strings
  add_track_fillers<float>(fillers, vars.halves, NAN, half);
  add_track_fillers<float>(fillers, vars.floats, NAN, standard);
  add_track_fillers<int>(fillers, vars.ints, -1, standard);
  add_track_fillers<unsigned char, unsigned char>(
    fillers, vars.uchars, 0, standard);
  add_track_fillers<char, char>(
    fillers, vars.chars, -1, standard);

  // use custom variables from the b-tagging inference code in
  // FlavorTagDiscriminants
  for (const std::string& name: vars.customs) {
    fillers.add(name, get_custom(name, config.ip_prefix), NAN, standard);
  }
  for (const std::string& name: vars.half_precision_customs) {
    fillers.add(name, get_custom(name, config.ip_prefix), NAN, half);
  }

  // add valid flag, for more robust selection, true for any track
  // that is defined.
  fillers.add("valid", [](const TrackOutputs&) { return true; }, false);

  // build the output dataset
  if (config.name.size() == 0) {
    throw std::logic_error("output name not specified");
  }
  if (config.output_size != 0) {
    m_hdf5_track_writer.reset(
      new TrackOutputWriter(
        output_file, config.name, fillers, config.output_size));
  }
}

BTagTrackWriter::~BTagTrackWriter() {
  if (m_hdf5_track_writer) m_hdf5_track_writer->flush();
}

BTagTrackWriter::BTagTrackWriter(BTagTrackWriter&&) = default;

void BTagTrackWriter::write(const BTagTrackWriter::Tracks& tracks,
                            const xAOD::Jet& jet) {
  if (m_hdf5_track_writer) {
    std::vector<TrackOutputs> trk_outputs;
    for (const auto* trk: tracks) {
      trk_outputs.push_back(TrackOutputs{trk, &jet});
    }
    m_hdf5_track_writer->fill(trk_outputs);
  }
}
void BTagTrackWriter::write_dummy() {
  if (m_hdf5_track_writer) {
    std::vector<TrackOutputs> trk_outputs;
    m_hdf5_track_writer->fill(trk_outputs);
  }
}

template<typename I, typename O>
void BTagTrackWriter::add_track_fillers(TrackConsumers& vars,
                                        const std::vector<std::string>& names,
                                        O def_value,
                                        H5Utils::Compression cmp) {

  for (const auto& btag_var: names) {
    SG::AuxElement::ConstAccessor<I> getter(btag_var);
    std::function<O(const TrackOutputs&)> filler =
      [getter](const TrackOutputs& trk) -> O {
        return getter(*trk.track);
      };
    vars.add(btag_var, filler, def_value, cmp);
  }
}
