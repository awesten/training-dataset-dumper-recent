#ifndef JET_LEPTON_DECAY_LABEEL_DECORATOR_HH
#define JET_LEPTON_DECAY_LABEEL_DECORATOR_HH

#include "xAODJet/JetFwd.h"
#include "xAODBTagging/BTaggingContainerFwd.h"
#include "AthLinks/ElementLink.h"
#include "xAODTruth/TruthParticle.h"

#include <string>

// this is a bare-bones example of a class that could be used to
// decorate a jet with additional information

class JetLeptonDecayLabelDecorator
{
public:
  JetLeptonDecayLabelDecorator(const std::string& prefix = "");

  // this is the function that actually does the decoration
  void decorate(const xAOD::Jet& jet) const;

private:
  // All this class does is apply a decoration, so all it needs to do
  // is contain one decorator. We could have also written a function
  // and statically initalized this, but static data in functions is
  // probably best avoided.
  SG::AuxElement::Decorator<int> m_deco;
                                             
  std::tuple<int,int> countLeptons(const std::vector<const xAOD::TruthParticle*> &Had, const xAOD::Jet& jet) const;

};

#endif
