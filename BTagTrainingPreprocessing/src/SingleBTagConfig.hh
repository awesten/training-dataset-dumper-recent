#ifndef SINGLEBTAG_CONFIG_HH
#define SINGLEBTAG_CONFIG_HH

#include "DL2Config.hh"

#include <string>
#include <vector>
#include <map>
#include <optional>
#include "TrackSelectorConfig.hh"
#include "TruthSelectorConfig.hh"
#include "TrackSortOrder.hh"
#include "BTagTrackWriterConfig.hh"
#include "HitWriterConfig.hh"
#include "HitDecoratorConfig.hh"

typedef std::map<std::string,std::vector<std::string>> VariableList;

enum class JetCleanOption {none, event, jet};

struct TrackConfig {
  size_t n_to_save;
  TrackSortOrder sort_order;
  TrackSelectorConfig selection;
  TrackWriterVariables variables;
  std::string input_name;
  std::string output_name;
  std::string ip_prefix;
};

struct TruthOutputConfig {
  std::string name;
  size_t n_to_save;
  TrackSortOrder sort_order;
};

struct TruthConfig {
  std::optional<TruthSelectorConfig> selection;
  std::vector<std::string> merge;
  float overlap_dr;
  std::string association_name;
  std::optional<TruthOutputConfig> output;
  std::optional<bool> decorate;
};

struct DecorateConfig {
  bool jet_aug;
  bool btag_jes;
  bool soft_muon;
  bool track_truth_info;
  bool track_sv_info;
  bool track_lepton_id;
  bool do_vrtrackjets_fix;  // PG: temporary fix for missing VR track jet decorators
  bool do_heavyions;
  bool lepton_decay_label;
};

struct JetCalibrationConfig {
  std::string collection;
  std::string configuration;
  std::string seq;
  std::string area;
};

struct SelectionConfig {
  bool truth_jet_matching;
  bool truth_primary_vertex_matching;
  size_t minimum_jet_constituents;
  float minimum_jet_pt;
  float maximum_jet_absolute_eta;
  float minimum_jvt;
  JetCleanOption jet_cleaning;
};

struct HitConfig {
  HitWriterConfig writer;
  HitDecoratorConfig decorator;
};

struct SingleBTagConfig {
  std::string jet_collection;
  std::optional<JetCalibrationConfig> calibration;
  SelectionConfig selection;
  std::string vertex_collection;
  std::string btagging_link;
  std::vector<DL2Config> dl2_configs;
  VariableList btag;
  std::map<std::string, std::string> default_flag_mapping;
  std::vector<TrackConfig> tracks;
  std::vector<TruthConfig> truths;
  std::optional<HitConfig> hits;
  DecorateConfig decorate;
};

SingleBTagConfig get_singlebtag_config(const std::string& config_file_name);


#endif
