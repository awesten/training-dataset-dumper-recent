#ifndef BTAG_TRACK_WRITER_HH
#define BTAG_TRACK_WRITER_HH

// Standard Library things
#include <string>
#include <vector>
#include <memory>

namespace H5 {
  class Group;
}
namespace xAOD {
  class TrackParticle_v1;
  typedef TrackParticle_v1 TrackParticle;
  class Jet_v1;
  typedef Jet_v1 Jet;
}

class BTagTrackWriterConfig;

class TrackOutputWriter;
class TrackConsumers;

struct TrackOutputs {
  const xAOD::TrackParticle* track;
  const xAOD::Jet* jet;
};

namespace H5Utils {
  enum class Compression;
}


class BTagTrackWriter
{
public:
  typedef std::vector<const xAOD::TrackParticle*> Tracks;
  BTagTrackWriter(
    H5::Group& output_file,
    const BTagTrackWriterConfig&);
  ~BTagTrackWriter();
  BTagTrackWriter(BTagTrackWriter&) = delete;
  BTagTrackWriter operator=(BTagTrackWriter&) = delete;
  BTagTrackWriter(BTagTrackWriter&&);
  void write(const BTagTrackWriter::Tracks& tracks, const xAOD::Jet& jet);
  void write_dummy();
private:
  template<typename I, typename O = I>
  void add_track_fillers(TrackConsumers&,
                         const std::vector<std::string>&,
                         O def_value,
                         H5Utils::Compression);
  std::unique_ptr<TrackOutputWriter> m_hdf5_track_writer;
};

#endif
