#include "JetLeptonDecayLabelDecorator.hh"

#include "TruthTools.hh"

#include "xAODBTagging/BTaggingUtilities.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthVertex.h"

// the constructor just builds the decorator
JetLeptonDecayLabelDecorator::JetLeptonDecayLabelDecorator(const std::string& prefix):
  m_deco(prefix + "LeptonDecayLabel")
{
}

// this call actually does the work on the jet
void JetLeptonDecayLabelDecorator::decorate(const xAOD::Jet& jet) const {

  //regular b-tagging :
  std::vector<const xAOD::TruthParticle*> ConeAssocBHad;
  const std::string labelB = "ConeExclBHadronsFinal";
  jet.getAssociatedObjects<xAOD::TruthParticle>(labelB, ConeAssocBHad);

  std::vector<const xAOD::TruthParticle*> ConeAssocCHad;
  const std::string labelC = "ConeExclCHadronsFinal";
  jet.getAssociatedObjects<xAOD::TruthParticle>(labelC, ConeAssocCHad); 

  // count all electrons and muons in b- and c- decay. the decay products of the
  // b hadron decay include those from the c hadron decay.
  auto [el_fromAll, mu_fromAll] = countLeptons(ConeAssocBHad, jet);

  // count all electrons and muons from the c decay only.
  auto [el_fromC, mu_fromC] = countLeptons(ConeAssocCHad, jet);

  // Get number of electrons/muon from b hadrons 
  int el_fromB = el_fromAll - el_fromC;
  int mu_fromB = mu_fromAll - mu_fromC;

  // Get extended label for semileptonic vs hadronic b/c decays
  int decay_label = 0;
  if (el_fromB > 0){decay_label += 11;}
  if (el_fromC > 0){decay_label += 11;}
  if (mu_fromB > 0){decay_label += 13;}
  if (mu_fromC > 0){decay_label += 13;}

  // The leptonic decay label is defined by the sum of the PdgId of the electrons and muons in the b- and c-hadron 
  // decay, while the occurrence of each lepton is only counted once per b-/c-decay. This means:
  // 0: all hadronic b- and c- decays
  // 11 (13): electrons (muons) in either b or c decay
  // 11 + 11 = 22: electrons in both decays
  // 11 + 13 = 24: electrons and muons in either b or c decay (not necessarily both in same decay)
  // 13 + 13 = 26: muons in both decays
  // 11 + 13 + 11(13) = 35 (37): both leptons in both decay, except no muons (electrons) in one of the decays
  // 11 + 11 + 13 + 13 = 48: both leptons in both decays
  m_deco(jet) = decay_label;
  
}

std::tuple<int,int> JetLeptonDecayLabelDecorator :: countLeptons(const std::vector<const xAOD::TruthParticle*> &Had, const xAOD::Jet &jet) const {
  int el_fromHad = 0;
  int mu_fromHad = 0;
  if ( Had.size()>0 ) {
    std::vector<int> HadIndices = truth::getDRSortedIndices(Had, jet);
    for ( unsigned int iHad=0; iHad < Had.size(); iHad++) {
      const xAOD::TruthParticle* myHad = Had.at(HadIndices[iHad]);
      std::vector<const xAOD::TruthParticle*> truthFromHad;
      truth::getAllChildren(myHad, truthFromHad);
      for(unsigned i=0; i< truthFromHad.size(); i++){
        const xAOD::TruthParticle* truth_particle = truthFromHad.at(i);
        if( std::abs(truth_particle->pdgId()) == 11) { el_fromHad += 1; }
        if( std::abs(truth_particle->pdgId()) == 13) { mu_fromHad += 1; }
      }  
    }
  }
  //return a tuple with the number of electrons and the number of muons from the b- or c-hadron
  return std::tuple<int,int> (el_fromHad, mu_fromHad);
}
