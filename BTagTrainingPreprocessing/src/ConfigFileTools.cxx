#include "ConfigFileTools.hh"
#include "BTagTrackWriterConfig.hh"

#include <cmath> // NAN
#include <set>

// Jan 7th, 2021: This define is needed before the json parser to
// silence a warning, it might go away with more recent versions of
// boost. Please consider removing the define below if you're reading
// this substantially after this date.
//
#define BOOST_BIND_GLOBAL_PLACEHOLDERS
#include <boost/property_tree/json_parser.hpp>

#include <nlohmann/json.hpp>

namespace {
  // Merging lists is just appending them for now
  void merge_lists(
    nlohmann::ordered_json& local,
    const nlohmann::ordered_json& fragment) {
    if (!local.is_array() || !fragment.is_array()) {
      std::string err = "list merge error: "
        + local.dump() + " + " + fragment.dump();
      throw std::logic_error(err);
    }
    for (const auto& item: fragment) {
      local.push_back(item);
    }
  }

  // Merge a new tree into the existing one
  //
  void merge_trees(nlohmann::ordered_json& local,
                   const nlohmann::ordered_json& fragment) {

    // For primative types there's nothing to do: the local one takes
    // precedence over any fragment.

    // For lists, just append the new values.
    if (local.is_array()) {
      merge_lists(local, fragment);
      return;
    } else if (local.is_object()) {
      // Objects are slightly more complicated: if the fragment
      // contains anything that isn't in the local tree, we add
      // it. Otherwise we attempt to merge it.
      for (auto& [key, value]: fragment.items()) {
        // Add new entries, only if they don't currently exist
        if (not local.contains(key)) {
          local[key] = value;
        } else {
          // If both trees have this object, try to merge them
          merge_trees(local.at(key), value);
        }
      }
    }
  }

}


namespace ConfigFileTools {

  // Expand file fragments
  //
  void combine_files(nlohmann::ordered_json& local,
                     std::filesystem::path config_path) {
    namespace fs = std::filesystem;

    // magic key for file fragments
    const std::string fragment_key = "file";

    // Start by building the local tree recursively.
    for (auto& sub: local) {
      if (sub.is_structured()) combine_files(sub, config_path);
    }

    // if there's no fragment at this level we're done
    if (!local.count(fragment_key)) return;

    // If we find a file fragment, we expand it. We look in two
    // places: the path relative to the working directory takes
    // precedence, but we'll also look relative to the configuration
    // path.
    fs::path fragment_path(local.at(fragment_key));
    if (!fs::exists(fragment_path)) {
      fragment_path = config_path / fragment_path;
    }
    std::ifstream stream(fragment_path.string());
    nlohmann::ordered_json fragment = nlohmann::json::parse(stream);

    // Recursively fill out the fragment, in case it has its own
    // sub-fragments. The search path for sub-fragments should include
    // the directory where this fragment lives.
    combine_files(fragment, fragment_path.parent_path());

    // Now we merge the fragment into the local tree. The local tree
    // will take precedence.
    merge_trees(local, fragment);

    // We're done with the fragment, get rid of any reference to it.
    local.erase(fragment_key);
  }

  // class to check that all keys are used
  //
  OptionalConfigurationObject::OptionalConfigurationObject(
    nlohmann::ordered_json cfg):
    m_cfg(cfg)
  {
  }
  void OptionalConfigurationObject::throw_if_unused(
    const std::string& type)
  {
    if (m_cfg.empty()) return;
    std::string problem = "Found unknown " + type + " in configuration";
    std::string sep = ": ";
    for (const auto& el: m_cfg.items()) {
      problem.append(sep + el.key());
      sep = ", ";
    }
    throw std::runtime_error(problem);
  }
  // specialization for const char* type, string literals are weird in C++
  std::string OptionalConfigurationObject::get(
    const std::string& key,
    const char* default_value) {
    return get<std::string>(key, default_value);
  }
  bool OptionalConfigurationObject::empty() const {
    return m_cfg.empty();
  }


  // converter to ptree, eventually we should migrate everything to
  // nlohmann
  ptree from_nlohmann(const nlohmann::ordered_json& in) {
    namespace pt = boost::property_tree;
    std::stringstream stream;
    stream << in.dump();
    pt::ptree out;
    pt::read_json(stream, out);
    return out;
  }


  bool boolinate(const boost::property_tree::ptree& pt,
                 const std::string& key) {
    std::string val = pt.get<std::string>(key);
    if (val == "true") return true;
    if (val == "false") return false;
    throw std::logic_error("'" + key + "' should be 'true' or 'false'");
  }
  bool boolinate_default(const boost::property_tree::ptree& pt,
                         const std::string& key,
                         bool default_value) {
    if (! pt.count(key) ) return default_value;
    return boolinate(pt, key);
  }
  std::vector<std::string> get_list(const ptree& pt) {
      std::vector<std::string> vars;
      for (const auto& var: pt) {
        vars.push_back(var.second.get_value<std::string>());
      }
      return vars;
  }
  std::set<std::string> get_key_set(const ptree& pt) {
    std::set<std::string> keys;
    for (const auto& pair: pt) {
      keys.insert(pair.first);
    }
    return keys;
  }
  void throw_if_any_keys_left(const std::set<std::string>& keys,
                              const std::string& type) {
    if (keys.empty()) return;
    std::string problem = "Found unknown " + type + " in configuration: ";
    for (const auto& k: keys) {
      problem.append(k);
      if (k != *keys.rbegin()) problem.append(", ");
    }
    throw std::runtime_error(problem);
  }
  MapOfLists get_variable_list(const boost::property_tree::ptree& pt) {
    MapOfLists var_map;
    for (const auto& node: pt) {
      var_map[node.first] = get_list(node.second);
    }
    return var_map;
  }
  TrackWriterVariables get_track_variables(const ptree& pt) {
    TrackWriterVariables vars;
    std::set<std::string> keys = get_key_set(pt);
    auto get = [&keys, &pt](const std::string& name) {
      if (keys.erase(name)) return get_list(pt.get_child(name));
      return std::vector<std::string>();
    };
#define FILL(field) vars.field = get(#field)
    FILL(uchars);
    FILL(chars);
    FILL(ints);
    FILL(halves);
    FILL(floats);
    FILL(customs);
    FILL(half_precision_customs);
#undef FILL
    throw_if_any_keys_left(keys, "track variable types");
    return vars;
  }

  float null_as_nan(const boost::property_tree::ptree& node,
                    const std::string& key) {
    const auto& child = node.get_child(key);
    if (child.data() == "null") return NAN;
    return child.get_value<float>();
  }

  DL2Config get_dl2_config(const nlohmann::ordered_json& node) {

    // keeps track of used keys
    ConfigFileTools::OptionalConfigurationObject nlocfg(node);

    // get the variable remapping
    const auto& remap_obj = nlocfg.get("remapping", nlohmann::json::object());
    const auto& remapping = remap_obj.get<std::map<std::string, std::string>>();

    // get the file path
    std::string nn_file_path = nlocfg.get<std::string>("nn_file_path", "");
    if (nn_file_path.empty()) throw std::runtime_error("No nn_file_path found in DL2 config");

    // check we used everything and return
    nlocfg.throw_if_unused("DL2 key");
    return {nn_file_path, remapping};
  }

  std::map<std::string, std::string> check_map_from(
    const MapOfLists& default_to_vals){
    std::map<std::string, std::string> out;
    for (const auto& pair: default_to_vals) {
      const std::string& check_var = pair.first;
      for (const auto& var: pair.second) {
        if (out.count(var)) {
          throw std::logic_error(var + " already in map");
        }
        out[var] = check_var;
      }
    }
    return out;
  }

}
