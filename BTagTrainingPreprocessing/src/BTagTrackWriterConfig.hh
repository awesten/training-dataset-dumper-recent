#ifndef BTAG_TRACK_WRITER_CONFIG_HH
#define BTAG_TRACK_WRITER_CONFIG_HH

#include <string>
#include <vector>

struct TrackWriterVariables {
  std::vector<std::string> uchars;
  std::vector<std::string> chars;
  std::vector<std::string> ints;
  std::vector<std::string> halves;
  std::vector<std::string> floats;
  // these variables come out of customGetter in FlavorTagDiscriminants
  std::vector<std::string> customs;
  std::vector<std::string> half_precision_customs;
};

struct BTagTrackWriterConfig {
  std::string name;
  std::string ip_prefix;

  std::size_t output_size;

  TrackWriterVariables variables;
};


#endif
