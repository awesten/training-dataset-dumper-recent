#ifndef FAT_JET_WRITER_HH
#define FAT_JET_WRITER_HH


namespace H5 {
  class Group;
}
namespace xAOD {
  class Jet_v1;
  typedef Jet_v1 Jet;
  class EventInfo_v1;
  typedef EventInfo_v1 EventInfo;
}

struct SubstructureAccessors;

struct FatJetWriterConfig;

class JetOutputWriter;
class JetOutputs;

namespace H5Utils {
  template<typename T> class Consumers;
}
using JetConsumers = H5Utils::Consumers<const JetOutputs&>;

#include <string>
#include <vector>
#include <map>

class FatJetWriter
{
public:
  FatJetWriter(
    H5::Group& output_file,
    const FatJetWriterConfig& jet);
  ~FatJetWriter();
  FatJetWriter(FatJetWriter&&);
  FatJetWriter(FatJetWriter&) = delete;
  FatJetWriter operator=(FatJetWriter&) = delete;
  void write_with_parent(const xAOD::Jet& jet,
                         const xAOD::Jet& parent,
                         const xAOD::EventInfo* = 0);
private:
  void add_substructure(JetConsumers&);
  JetOutputWriter* m_hdf5_jet_writer;
  SubstructureAccessors* m_ssa;
};


#endif
