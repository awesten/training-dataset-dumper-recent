#include "TrackLeptonDecorator.hh"

#include "xAODEgamma/ElectronxAODHelpers.h"

#include "xAODEgamma/ElectronxAODHelpers.h"
#include "ElectronPhotonSelectorTools/ElectronSelectorHelpers.h"

void check_rc(StatusCode code) {
  if (!code.isSuccess()) throw std::runtime_error("bad return code");
}

// the constructor just builds the decorator
TrackLeptonDecorator::TrackLeptonDecorator(const std::string& prefix):
  m_track_lepton_id(prefix + "leptonID"),
  m_electronID_tool("AsgElectronLikelihoodTool/electronID_tool")
 {
  check_rc(m_electronID_tool.setProperty("WorkingPoint", m_electronID_wp));
  check_rc(m_electronID_tool.initialize());
 }

// this call actually does the work on the track
void TrackLeptonDecorator::decorate(const xAOD::TrackParticleContainer& tracks, const xAOD::MuonContainer& muons, const xAOD::ElectronContainer& electrons) const {

  // give all tracks a default starting value
  for ( const xAOD::TrackParticle* track : tracks ) {
    m_track_lepton_id(*track) = 0;
  }

    // loop over electrons
    for ( const auto electron : electrons ) {

      // get associated InDet track (not the GSF track which is likely to have improved parameters,
      // more info: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/EGammaD3PDtoxAOD#TrackParticle)
      const xAOD::TrackParticle* track = xAOD::EgammaHelpers::getOriginalTrackParticle(electron);
      if ( !track ) { continue; }

      // apply electron ID requirement
      bool LH_selection = (bool)m_electronID_tool.accept(electron);
      if (!LH_selection) { continue; }

      // decorate the track
      m_track_lepton_id(*track) = -11 * electron->charge();
    }

    // loop over muons - do it last in case we have a track that was used in the 
    // reconstruction of an electron and a muon (which can happen in rare cases)
    for ( const auto muon : muons ) {

      // minimal quality requirement: check we have a combined muon
      if (muon->muonType() != xAOD::Muon::Combined) { continue; }

      // get associated InDet track
      auto track_link = muon->inDetTrackParticleLink();
      if ( !track_link.isValid() ) { continue; }
      auto track = *track_link;

      // decorate the track
      m_track_lepton_id(*track) = -13 * muon->charge();
    }



}
