#ifndef SINGLE_BTAG_ALG_HH
#define SINGLE_BTAG_ALG_HH

#include "H5FileSvc.h"

#include "xAODBTagging/BTaggingContainer.h"
#include "xAODJet/JetContainer.h"

#include "AthenaBaseComps/AthAlgorithm.h"
#include "StoreGate/ReadHandleKey.h"
#include "StoreGate/ReadDecorHandleKey.h"
#include "GaudiKernel/ServiceHandle.h"

#include <memory>

class SingleBTagTools;
class SingleBTagConfig;
class SingleBTagOutputs;
namespace H5 {
  class H5File;
}

class SingleBTagAlg: public AthAlgorithm
{
public:
  SingleBTagAlg(const std::string& name, ISvcLocator* pSvcLocator);
  ~SingleBTagAlg();

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;
private:
  std::string m_config_file_name;
  std::string m_metadata_file_name;

  std::unique_ptr<SingleBTagConfig> m_config;

  std::unique_ptr<SingleBTagTools> m_tools;
  std::unique_ptr<SingleBTagOutputs> m_outputs;

  // stuff for data dependencies
  SG::ReadHandleKey<xAOD::JetContainer> m_jetKey{this, "jetKey", "", ""};
  std::vector<std::unique_ptr<SG::ReadDecorHandleKey<xAOD::BTagging>>> m_bDec;
  ServiceHandle<H5FileSvc> m_output_svc {
    this, "output", "", "output file service"};
  Gaudi::Property<std::string> m_h5_dir {
    this, "group", "", "Group in H5 file"};

};

#endif // SINGLE_BTAG_ALG_HH
