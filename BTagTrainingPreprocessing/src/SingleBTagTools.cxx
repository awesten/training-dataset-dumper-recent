#include "SingleBTagTools.hh"

#include "SingleBTagConfig.hh"
#include "BTagJetWriterConfig.hh"
#include "BTagTrackWriterConfig.hh"

#include "H5Cpp.h"

namespace {
  std::vector<std::string> get(const VariableList& v, const std::string& k) {
    if (v.count(k)) return v.at(k);
    return {};
  }
  void check_rc(StatusCode code) {
    if (!code.isSuccess()) throw std::runtime_error("bad return code");
  }


  BTagJetWriterConfig jwConfig(const SingleBTagConfig& jobcfg) {
    BTagJetWriterConfig jet_cfg;
    jet_cfg.event_info = get(jobcfg.btag, "event");
    jet_cfg.char_variables = get(jobcfg.btag, "chars");
    jet_cfg.uchar_variables = get(jobcfg.btag, "uchars");
    jet_cfg.jet_int_variables = get(jobcfg.btag, "jet_int_variables");
    jet_cfg.jet_float_variables = get(jobcfg.btag,"jet_floats");
    jet_cfg.jet_char_variables = get(jobcfg.btag,"jet_chars");
    jet_cfg.int_as_float_variables = get(jobcfg.btag, "ints_as_float");
    jet_cfg.float_variables = get(jobcfg.btag, "floats");
    jet_cfg.double_variables = get(jobcfg.btag, "doubles");
    jet_cfg.variable_maps.replace_with_defaults_checks
      = jobcfg.default_flag_mapping;
    jet_cfg.variable_maps.rename = {}; // please don't use this :(
    jet_cfg.n_jets_per_event = 0;
    jet_cfg.name = "jets";
    jet_cfg.btagging_link = jobcfg.btagging_link;
    return jet_cfg;
  }
  BTagTrackWriterConfig trackWriterConfig(const TrackConfig& jobcfg) {
    BTagTrackWriterConfig track_cfg;
    track_cfg.name = jobcfg.output_name;
    track_cfg.ip_prefix = jobcfg.ip_prefix;
    track_cfg.variables = jobcfg.variables;
    track_cfg.output_size = {jobcfg.n_to_save};
    return track_cfg;
  }
}

TrackToolkit::TrackToolkit(const TrackConfig& cfg, H5::Group& output):
  selector(cfg.selection, cfg.input_name),
  sorted(trackSort(cfg.sort_order, cfg.ip_prefix)),
  writer(output, trackWriterConfig(cfg)),
  n_tracks_decorator("n_" + cfg.output_name)
{
}
TrackToolkit::TrackToolkit(TrackToolkit&&) = default;


SingleBTagTools::Accessors::Accessors(const SingleBTagConfig& cfg):
  eventClean_looseBad("DFCommonJets_eventClean_LooseBad"),
  jvt("Jvt")
{
  if (cfg.btagging_link.empty())
    btaggingLink = [] (const xAOD::Jet&) { return nullptr; };
  else{
    SG::AuxElement::ConstAccessor<ElementLink<xAOD::BTaggingContainer>>a(cfg.btagging_link);
    btaggingLink = [a] (const xAOD::Jet & j) { return *a(j); };
  }
}

SingleBTagTools::SingleBTagTools(const SingleBTagConfig& jobcfg):
  calibration_tool("JetCalibrationTool"),
  cleaning_tool("JetCleaningTool", JetCleaningTool::LooseBad, false),
#ifndef DISABLE_JVT
  jvttool("JetVertexTaggerTool"),
#endif
  shallow_copier(jobcfg.btagging_link),
  muon_augmenter("Muons"),
  acc(jobcfg)
{
  if (jobcfg.calibration){
    const JetCalibrationConfig& cfg = *jobcfg.calibration;
    JetCalibrationTool& jct = calibration_tool;
    check_rc( jct.setProperty("JetCollection", cfg.collection));
    check_rc( jct.setProperty("ConfigFile", cfg.configuration) );
    check_rc( jct.setProperty("CalibSequence", cfg.seq) );
    check_rc( jct.setProperty("CalibArea", cfg.area) );
    check_rc( jct.setProperty("IsData", false) );
    check_rc( jct.initialize() );
  }
  check_rc( cleaning_tool.setProperty("JetContainer",
                                      jobcfg.jet_collection) );
  check_rc( cleaning_tool.initialize() );

  check_rc( jvttool.setProperty("JetContainer",
                                jobcfg.jet_collection) );
  check_rc( jvttool.initialize() );

  for (const auto& cfg: jobcfg.dl2_configs) {
    using FlavorTagDiscriminants::FlipTagConfig;
    std::string path = cfg.nn_file_path;
    std::cout << "loading " << path << std::endl;
    dl2s.emplace_back(path, FlipTagConfig::STANDARD, cfg.remapping);
  }

  // instantiate truth particle decorators
  for (const auto& cfg: jobcfg.truths) {
    if (cfg.selection) {
      truth_associators.emplace_back(
        cfg.association_name, *cfg.selection);
    }
    if (!cfg.merge.empty()) {
      jet_truth_mergers.emplace_back(cfg.merge, cfg.association_name);
    }
    // add overlap checks
    if (cfg.overlap_dr > 0) {
      SG::AuxElement::ConstAccessor<JetTruthAssociator::PartLinks> acc(
        cfg.association_name);
      overlap_checks.emplace_back(
        [acc, dr=cfg.overlap_dr](const xAOD::Jet& j) {
          for (const JetTruthAssociator::PartLink& tpl: acc(j)) {
            if (!tpl.isValid()) {
              throw std::runtime_error("invalid particle link");
            }
            const xAOD::IParticle* part = *tpl;
            if (part->p4().DeltaR(j.p4()) < dr) return true;
          }
          return false;
        });
    }
    if (cfg.decorate) {
      std::string out_name;
      if (cfg.output) { out_name = cfg.output->name; }
      else { out_name = cfg.association_name; }
      jetTruthSummaryDecorators.emplace_back(cfg.association_name, out_name);
    }
  }
  if (jobcfg.hits) {
    hit_decorator.reset(new HitDecorator(jobcfg.hits->decorator));
  }
}

SingleBTagOutputs::SingleBTagOutputs(const SingleBTagConfig& jobcfg,
                                     H5::Group& output):
  jet_writer(output, jwConfig(jobcfg))
{
  for (const TrackConfig& cfg: jobcfg.tracks) {
    tracks.emplace_back(cfg, output);
  }
  for (const TruthConfig& cfg: jobcfg.truths) {
    if (cfg.output) {
      const TruthOutputConfig& oc = *cfg.output;
      // TODO: TruthWriter needs a config struct
      truths.emplace_back(output, oc.n_to_save,
                          cfg.association_name, oc.name,
                          oc.sort_order);
    }
  }
  if (jobcfg.hits && jobcfg.hits->writer.output_size > 0) {
    hits.reset(new HitWriter(output, jobcfg.hits->writer));
  }

}
