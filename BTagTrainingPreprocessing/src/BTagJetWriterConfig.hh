#ifndef BTAG_JET_WRITER_CONFIG_HH
#define BTAG_JET_WRITER_CONFIG_HH

#include <map>
#include <string>
#include <vector>

struct BTagVariableMaps {
  std::map<std::string, std::string> replace_with_defaults_checks;
  std::map<std::string, std::string> rename;
};

struct BTagJetWriterBaseConfig {
  BTagJetWriterBaseConfig();
  std::string name;
  std::vector<std::string> event_info;
  BTagVariableMaps variable_maps;
  std::string btagging_link;

  std::vector<std::string> jet_int_variables;
  std::vector<std::string> jet_float_variables;
  std::vector<std::string> jet_char_variables;

  std::vector<std::string> char_variables;
  std::vector<std::string> uchar_variables;
  std::vector<std::string> int_variables;
  std::vector<std::string> int_as_float_variables;
  std::vector<std::string> float_variables;
  std::vector<std::string> double_variables;
};

struct BTagJetWriterConfig: public BTagJetWriterBaseConfig {
  BTagJetWriterConfig();
  size_t n_jets_per_event;
};

struct SubjetWriterConfig: public BTagJetWriterBaseConfig {
  SubjetWriterConfig();
  bool write_kinematics_relative_to_parent;
};

#endif
