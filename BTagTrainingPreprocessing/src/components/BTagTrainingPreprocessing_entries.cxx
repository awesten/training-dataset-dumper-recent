#include "src/SingleBTagAlg.h"
#include "src/TrackSystematicsAlg.h"
#include "src/TriggerJetGetterAlg.h"
#include "src/TriggerBTagMatcherAlg.h"
#include "src/H5FileSvc.h"

DECLARE_COMPONENT(SingleBTagAlg)
DECLARE_COMPONENT(TrackSystematicsAlg)
DECLARE_COMPONENT(TriggerJetGetterAlg)
DECLARE_COMPONENT(TriggerBTagMatcherAlg)
DECLARE_COMPONENT(H5FileSvc)
