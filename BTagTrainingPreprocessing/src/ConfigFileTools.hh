#ifndef CONFIG_FILE_TOOLS_HH
#define CONFIG_FILE_TOOLS_HH

#include "DL2Config.hh"

#include <boost/property_tree/ptree.hpp>
#include <nlohmann/json.hpp>
#include <filesystem>
#include <string>
#include <vector>
#include <map>
#include <set>

class TrackWriterVariables;

namespace ConfigFileTools {

  using boost::property_tree::ptree;

  typedef std::map<std::string,std::vector<std::string>> MapOfLists;

  // Expand file fragments
  //
  // If the local json tree has an entry called "file" we merge
  // anything defined in that "fragment" to the local tree.
  //
  // - The merge is recursive, and fragments can have sub-fragments.
  //
  // - When merging objects, local values take precidence.
  //
  // - Lists are merged by concatenating them. Trying to merge a list
  //   with anything else will throw an exception.
  //
  // - The first argument is the base json object. If no fragments are
  //   found it won't be altered.
  //
  // - The second argument points to a root directory to search for
  //   fragments. This path is only used if the fragment path relative
  //   to the working directory is invalid.
  //
  void combine_files(nlohmann::ordered_json& local,
                     std::filesystem::path config_path);

  // This class insists that every field in a json object be checked
  class OptionalConfigurationObject
  {
  public:
    OptionalConfigurationObject(nlohmann::ordered_json cfg);
    //
    // this function should be called after all the keys are checked,
    // will throw a std::runtime_error if some remain.
    void throw_if_unused(const std::string& type = "keys");
    //
    // the main getter function
    template <typename T = nlohmann::ordered_json>
    T get(const std::string& key, T default_value = nullptr);
    //
    // we need this special overload to support string literals
    std::string get(const std::string& key, const char* default_value);
    //
    // Required inputs
    template <typename T>
    T require(const std::string& key);
    //
    // empty returns true for empty array, null, or empty object
    bool empty() const;
  private:
    nlohmann::ordered_json m_cfg;
  };

  // workaround while we migrate the remaining code to nlohmann
  ptree from_nlohmann(const nlohmann::ordered_json&);

  // helper functions for optional keys
  std::set<std::string> get_key_set(const ptree& pt);
  void throw_if_any_keys_left(const std::set<std::string>& keys,
                              const std::string& type = "keys");

  MapOfLists get_variable_list(const boost::property_tree::ptree& pt);
  bool boolinate(const boost::property_tree::ptree& pt,
                 const std::string& key);
  bool boolinate_default(const boost::property_tree::ptree& pt,
                         const std::string& key,
                         bool default_value = false);
  float null_as_nan(const boost::property_tree::ptree& node,
                    const std::string& key);

  DL2Config get_dl2_config(const nlohmann::ordered_json& node);

  std::map<std::string, std::string> check_map_from(const MapOfLists&);

  TrackWriterVariables get_track_variables(const ptree& pt);
}

// Implementation of template above

namespace ConfigFileTools {
  template <typename T>
  T OptionalConfigurationObject::get(const std::string& key, T default_value)
  {
    if (m_cfg.contains(key)) {
      const auto& nloval = m_cfg.at(key);
      // if the value of this key is null we return the default
      T return_value = default_value;
      if (!nloval.is_null()) return_value = nloval;
      m_cfg.erase(key);
      return return_value;
    } else {
      return default_value;
    }
  }
  template <typename T>
  T OptionalConfigurationObject::require(const std::string& key)
  {
    if (m_cfg.contains(key)) {
      nlohmann::ordered_json& nloval = m_cfg.at(key);
      if (nloval.is_null()) {
        throw std::runtime_error("required value '" + key + "' is null");
      }
      T val = nloval;
      m_cfg.erase(key);
      return val;
    } else {
      throw std::runtime_error("can't find required key '" + key + "'");
    }
  }
}

#endif
