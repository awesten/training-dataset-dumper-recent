#include "JetConstWriter.hh"
#include "JetConstWriterConfig.hh"
#include "HDF5Utils/Writer.h"

// Less Standard Libraries (for atlas)
#include "H5Cpp.h"

// ATLAS things
#include "xAODPFlow/TrackCaloCluster.h"
#include "xAODJet/Jet.h"

typedef std::function<float(const ConstOutputs&)> FloatFiller;
typedef std::function<int(const ConstOutputs&)> IntFiller;
class ConstConsumers: public H5Utils::Consumers<const ConstOutputs&> {};
class ConstOutputWriter: public H5Utils::Writer<1,const ConstOutputs&>
{
public:
  ConstOutputWriter(H5::Group& file,
                    const std::string& name,
                    const ConstConsumers& cons,
                    size_t size):
    H5Utils::Writer<1,const ConstOutputs&>(file, name, cons, {{size}}) {}
};

JetConstWriter::JetConstWriter(
  H5::Group& output_file,
  const JetConstWriterConfig& config):
  m_hdf5_const_writer(nullptr)
{

  ConstConsumers fillers;

  // hard coded 4 momentum fillers
  FloatFiller pt = [](const ConstOutputs& t) -> float {
    return t.tcc->pt();
  };
  fillers.add("pt", pt, NAN);
  FloatFiller eta = [](const ConstOutputs& t) -> float {
    return t.tcc->eta();
  };
  fillers.add("eta", eta, NAN);

  FloatFiller dphi = [](const ConstOutputs& t) -> float {
    return t.jet->p4().DeltaPhi(t.tcc->p4());
  };
  fillers.add("dphi", dphi, NAN);

  FloatFiller m = [](const ConstOutputs& t) -> float {
    return t.tcc->m();
  };
  fillers.add("m", m, NAN);

  IntFiller taste = [](const ConstOutputs& t) -> int {
    return t.tcc->taste();
  };
  fillers.add("taste", taste, NAN);

  // build the output dataset
  assert(config.name.size() > 0);
  if (config.output_size > 0) {
    m_hdf5_const_writer = new ConstOutputWriter(output_file, config.name,
                                                fillers, config.output_size);
  }
}

JetConstWriter::~JetConstWriter() {
  if (m_hdf5_const_writer) m_hdf5_const_writer->flush();
  delete m_hdf5_const_writer;
}

void JetConstWriter::write(const JetConstWriter::TCCs& tccs,
                            const xAOD::Jet& jet) {
  if (m_hdf5_const_writer) {
    std::vector<ConstOutputs> const_outputs;
    for (const auto* tcc: tccs) {
      const_outputs.push_back(ConstOutputs{tcc, &jet});
    }
    m_hdf5_const_writer->fill(const_outputs);
  }
}
void JetConstWriter::write_dummy() {
  if (m_hdf5_const_writer) {
    std::vector<ConstOutputs> const_outputs;
    m_hdf5_const_writer->fill(const_outputs);
  }
}