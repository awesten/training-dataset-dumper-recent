#include "processSingleBTagEvent.hh"

// the implementation is the same for every signature, we just have to
// recompile it a few times below. We do it here to avoid having the
// template headers interfere with anything that calls this function.
#include "SingleBTagTools.hh"
#include "SingleBTagConfig.hh"
#include "BJetShallowCopier.hh"
#include "JetTruthAssociator.hh"
#include "TruthTools.hh"
#include "cleanHits.hh"

#include "xAODRootAccess/TEvent.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackMeasurementValidationContainer.h"

#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"

#include "xAODHIEvent/HIEventShapeAuxContainer.h"
#include "xAODHIEvent/HIEventShapeContainer.h"

#include "PathResolver/PathResolver.h"

namespace {

  void check_rc(StatusCode code) {
    if (!code.isSuccess()) throw std::runtime_error("bad return code");
  }

  constexpr float operator "" _GeV(unsigned long long d) { return d*1e3; }
  constexpr float operator "" _GeV(long double d) { return d*1e3; }

  const xAOD::Vertex* primary(const xAOD::VertexContainer& vertices) {
    if (vertices.size() == 0) {
      throw std::runtime_error("no primary vertices");
    }
    for ( const xAOD::Vertex *vertex : vertices ) {
      if ( vertex->vertexType() == xAOD::VxType::PriVtx ) {
        return vertex;
      }
    }
    // if we find nothing else this should be the beam spot
    return vertices.front();
  }
}

// this is the templated code, the concrete instances are below
template <typename Event>
void processSingleBTagEventImpl(Event& event,
                                const SingleBTagConfig& jobcfg,
                                const SingleBTagTools& tools,
                                SingleBTagOutputs& out) {
  const xAOD::EventInfo *event_info = nullptr;
  check_rc( event.retrieve(event_info, "EventInfo") );

   if (jobcfg.decorate.do_heavyions) {
    //retrieve FCal ET for collisions centrality percentiles. do_heavyions(true) is needed.
    const xAOD::HIEventShapeContainer* calos = 0;
    check_rc( event.retrieve(calos, "CaloSums") );
    tools.dec.fcal_et_tev(*event_info) = calos->at(5)->et() * 1e-6; // MeV->TeV
  }

  // jet event cleaning: do not process events with bad jets
  if (jobcfg.selection.jet_cleaning == JetCleanOption::event) {
    bool result = tools.acc.eventClean_looseBad(*event_info);
    // do not process event, if event-level jet cleaning flag is not passed
    if (!result) return;
  }

  // we'll need truth particles and event info
  const xAOD::TruthParticleContainer* tpc = nullptr;
  if (!tools.truth_associators.empty()){
    try {
      check_rc( event.retrieve(tpc, "TruthParticles") );
      std::vector<const xAOD::TruthParticle*> truth_particles(
        tpc->begin(), tpc->end());
      truth::getLeptonsFromWZ(truth_particles);
      out.truth_counts.n_successful_truth_reads++;
    } catch (truth::TruthRecordError& err) {
      std::cerr << getEventInfoString(err, *event_info) << std::endl;
      out.truth_counts.n_failed_truth_reads++;
      return;
    }
  }

  // read the jets
  const xAOD::JetContainer *raw_jets = nullptr;
  check_rc( event.retrieve(raw_jets, jobcfg.jet_collection) );

  const xAOD::TrackMeasurementValidationContainer* hits_orig = nullptr;
  std::vector<const xAOD::TrackMeasurementValidation*> hits;
  // Check whether we want to save the hits with HitWriter or we want
  // to decorate the jets with the hit multiplicities
  if (jobcfg.hits) {
    check_rc(event.retrieve(hits_orig, "JetAssociatedPixelClusters"));
    // Remove hits overlaping in phi and fakes
    hits = cleanHits(hits_orig, jobcfg.hits->writer.save_only_clean_hits);
  }

  // first loop: add decorations to jets. These have to be done
  // before calibration to be consistent with reconstruction
  auto [jets, aux] = tools.shallow_copier.shallowCopyBJets(*raw_jets);
  for (const xAOD::Jet* uncalib_jet: *jets) {

    // this is more important stuff
    const xAOD::BTagging* btag = tools.acc.btaggingLink(*uncalib_jet);

    if (jobcfg.decorate.jet_aug) {
      tools.jet_augmenter.augment(*btag, *btag);
    }
    if (jobcfg.decorate.soft_muon) {
      tools.muon_augmenter.augment(*btag);
    }
    if (jobcfg.decorate.btag_jes){
      tools.jet_augmenter.augmentBtagJes(*btag, *btag);
    }

    for (const auto& dl2: tools.dl2s) {
      dl2.decorate(*btag);
    }

    // decorate truth particles
    for (const auto& decor: tools.truth_associators) {
      decor.decorate(*uncalib_jet, tpc);
    }
    // merge truth particles
    for (const auto& merger: tools.jet_truth_mergers) {
      merger.decorate(*uncalib_jet);
    }

    // PG: temporary fix for missing VR track jet decorators
    // Fixed in https://gitlab.cern.ch/atlas/athena/-/merge_requests/49966
    if (jobcfg.decorate.do_vrtrackjets_fix && (jets->size() == 1)) {
      tools.dec.trackjet_rel_dR(*uncalib_jet) = INFINITY;
      tools.dec.trackjet_abs_dR(*uncalib_jet) = INFINITY;
    }
    // PG: end of temporary fix for missing VR track jet decorators
  }

  if (jobcfg.calibration) {
    check_rc(tools.calibration_tool.applyCalibration(*jets));
  }

  // sort jets by descending pt
  // we make a new container first to preserve the indices
  std::vector<const xAOD::Jet*> sorted_jets(jets->begin(), jets->end());
  std::sort(sorted_jets.begin(), sorted_jets.end(),
            [](const auto* j1, const auto* j2) {
              return j1->pt() > j2->pt();
            });

  // we need the pv to add the correct hits to jets
  const xAOD::VertexContainer* primary_vertices = nullptr;
  check_rc( event.retrieve(primary_vertices, jobcfg.vertex_collection));
  const xAOD::Vertex* pv = primary(*primary_vertices);
  if (jobcfg.hits) {
    for (const xAOD::Jet* jet: sorted_jets) {
      tools.hit_decorator->decorate(*jet, hits, *pv);
    }
  }

  if (jobcfg.decorate.lepton_decay_label){
    for (const xAOD::Jet* jet: sorted_jets) {
      tools.jet_lepton_decay_label_decorator.decorate(*jet);
    }
  }

  // decorate tracks with reco lepton ID
  if (jobcfg.decorate.track_lepton_id) {
    const xAOD::TrackParticleContainer* tracks = nullptr;
    const xAOD::MuonContainer* muons = nullptr;
    const xAOD::ElectronContainer* electrons = nullptr;
    check_rc(event.retrieve(tracks, "InDetTrackParticles"));
    check_rc(event.retrieve(muons, "Muons"));
    check_rc(event.retrieve(electrons, "Electrons"));
    tools.trkLeptonDecorator.decorate(*tracks, *muons, *electrons);
  }

  // save some primary vertex information on eventinfo
  tools.dec.n_primary_vertices(*event_info) = primary_vertices->size();
  tools.dec.primary_vertex_detector_z(*event_info) = pv->z();
  tools.dec.primary_vertex_detector_z_uncertainty(*event_info) = (
    std::sqrt(pv->covariancePosition()(2,2)));
  const xAOD::TruthEventContainer* truthEventContainer = nullptr;
  const xAOD::TruthVertex* truth_PV = nullptr;
  if ( jobcfg.decorate.track_truth_info ||
       jobcfg.selection.truth_primary_vertex_matching ) {
    check_rc( event.retrieve(truthEventContainer, "TruthEvents"));
    // truthEventContainer always has size == 1?
    truth_PV = truthEventContainer->at(0)->truthVertex(0);
  }

  // Reject events with PV badly reconstructed
  if (jobcfg.selection.truth_primary_vertex_matching) {
    if (std::abs( pv->z() - truth_PV->z() ) > 0.1) return;
  }

  // Retrieve AntiKt4TruthJets collection if we are doing truth jet matching
  const xAOD::JetContainer *truth_jets = nullptr;
  if (jobcfg.selection.truth_jet_matching) {
    check_rc( event.retrieve(truth_jets, "AntiKt4TruthJets"));
  }

  // second loop: select calibrated jets and write out to HDF5
  unsigned int rank = 0;
  std::vector<const xAOD::Jet*> jets_to_write;
  const SelectionConfig& sel = jobcfg.selection;
  for (const xAOD::Jet* calib_jet : sorted_jets) {

    // overlap removal
    const auto& vetos = tools.overlap_checks;
    auto veto_check = [&j=*calib_jet](auto& f) {return f(j); };
    if (std::any_of(vetos.begin(), vetos.end(), veto_check)) continue;

    // get the b-tagging object
    const xAOD::BTagging* btag = tools.acc.btaggingLink(*calib_jet);

    // decorate jet with lepton MCTC truth info
    if (jobcfg.decorate.soft_muon) {
      tools.lepTruthDecorator.decorate(*btag, *calib_jet);
    }

    // decorate jet with summary info about associated truth collections
    for (const auto& dec: tools.jetTruthSummaryDecorators) {
      dec.decorate(*calib_jet);
    }

    tools.dec.jet_rank(*calib_jet) = rank++;

    // don't bother using JVT if it's set to -inf
    bool use_jvt = !(std::isinf(sel.minimum_jvt) && sel.minimum_jvt < 0);
    if (use_jvt) {
      float jvt = NAN;
      if (jobcfg.calibration) {
        // if we're calibrating jets we need to check the JVT again
        jvt = tools.jvttool.updateJvt(*calib_jet);
      } else {
        jvt = tools.acc.jvt(*calib_jet);
      }
      bool fail_jvt = (
        calib_jet->pt() > 20_GeV &&
        calib_jet->pt() < 60_GeV &&
        std::abs(calib_jet->eta()) < 2.4 &&
        jvt < sel.minimum_jvt );
      if (fail_jvt) continue;
      tools.dec.jvt(*calib_jet) = jvt;
    }

    // only do jet-level jet cleaning if not doing event-level jet cleaning
    if (sel.jet_cleaning == JetCleanOption::jet) {
      if (!tools.cleaning_tool.keep(*calib_jet)) continue;
    }

    // pt and eta requirements
    if (calib_jet->pt() < sel.minimum_jet_pt) continue;
    if (std::abs(calib_jet->eta()) > sel.maximum_jet_absolute_eta) continue;

    // minimum constituent requirement
    if (calib_jet->numConstituents() < sel.minimum_jet_constituents) continue;

    if (sel.truth_jet_matching) {
      if (!truth::passed_truth_jet_matching(*calib_jet, *truth_jets)) {
        continue;
      }
    }

    // write out tracks associated with jets
    for (auto& tracktool: out.tracks ) {
      const xAOD::Jet* uncalib_jet = raw_jets->at(calib_jet->index());
      auto tracks = tracktool.selector.get_tracks(*uncalib_jet);
      if ( jobcfg.decorate.track_sv_info ) {
            tools.trkVertexDecorator.decorateAll(tracks, *btag, *pv);
      }
      if ( jobcfg.decorate.track_truth_info ) {
            tools.trkTruthDecorator.decorateAll(tracks, truth_PV);
      }

      const auto sorted_tracks = tracktool.sorted(tracks, *uncalib_jet);
      tracktool.writer.write(sorted_tracks, *uncalib_jet);
      tracktool.n_tracks_decorator(*calib_jet) = tracks.size();
    }

    // write hits
    if (out.hits) {
      const xAOD::Jet* uncalib_jet = raw_jets->at(calib_jet->index());
      out.hits->write(hits, *uncalib_jet, *pv);
    }

    // write truth particles
    for (auto& truthtool: out.truths) {
      const xAOD::Jet* uncalib_jet = jets->at(calib_jet->index());
      truthtool.write(*uncalib_jet);
    }

    // collect jets for output
    jets_to_write.push_back(calib_jet);
  }

  // write out jets
  if (!jets_to_write.empty()) {
    out.jet_writer.write(jets_to_write, event_info);
  }

}

// Concrete versions of the templated function above. These are the
// ones that are exposed to be used in other files.
void processSingleBTagEvent(xAOD::TEvent& e,
                            const SingleBTagConfig& c,
                            const SingleBTagTools& t,
                            SingleBTagOutputs& o) {
  processSingleBTagEventImpl(e, c, t, o);
}

// StoreGateSvc isn't defined in AnalysisBase...
#ifndef XAOD_STANDALONE
void processSingleBTagEvent(StoreGateSvc& e,
                            const SingleBTagConfig& c,
                            const SingleBTagTools& t,
                            SingleBTagOutputs& o) {
  processSingleBTagEventImpl(e, c, t, o);
}
#else
// ... but SgTEvent is
void processSingleBTagEvent(asg::SgTEvent& e,
                            const SingleBTagConfig& c,
                            const SingleBTagTools& t,
                            SingleBTagOutputs& o) {
  processSingleBTagEventImpl(e, c, t, o);
}
#endif  // XAOD_STANDALONE
