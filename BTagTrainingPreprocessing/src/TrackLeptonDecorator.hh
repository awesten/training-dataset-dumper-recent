#ifndef TRACK_LEPTON_DECORATOR_HH
#define TRACK_LEPTON_DECORATOR_HH

#include "xAODJet/Jet.h"
#include "xAODBTagging/BTaggingUtilities.h"

#include "xAODBTagging/SecVtxHelper.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODBTagging/BTagVertexContainer.h"

#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"

#include <string>

// this is a bare-bones example of a class that could be used to
// decorate a track with additional information

class TrackLeptonDecorator
{
public:
  TrackLeptonDecorator(const std::string& decorator_prefix = "");

  // this is the function that actually does the decoration
  void decorate(const xAOD::TrackParticleContainer& tracks, const xAOD::MuonContainer& muons, const xAOD::ElectronContainer& electrons) const;

private:
  // All this class does is apply a decoration, so all it needs to do
  // is contain one decorator. We could have also written a function
  // and statically initalized this, but static data in functions is
  // probably best avoided.

  SG::AuxElement::Decorator<char> m_track_lepton_id;

  // electron ID tool
  std::string m_electronID_wp = "VeryLooseLHElectron";
  AsgElectronLikelihoodTool m_electronID_tool;
};

#endif
