#!/usr/bin/env python3

"""
Dump some ftag trigger info!

This runs quite a few algorithms upstream of the dumping code, to
extract trigger jets from the trigger decisions, truth label them, and
compare them to offline reconstructed jets.
"""

from BTagTrainingPreprocessing import trigger as trig
from BTagTrainingPreprocessing import dumper

from AthenaConfiguration.MainServicesConfig import (
    MainServicesCfg as getConfig)
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
from AthenaServices.MetaDataSvcConfig import MetaDataSvcCfg
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator

from AthenaConfiguration.ComponentFactory import CompFactory
from GaudiKernel.Configurable import DEBUG, INFO, VERBOSE

from argparse import ArgumentParser
from itertools import chain
import sys

def get_args():
    default_chain = 'HLT_j20_0eta290_020jvt_pf_ftf_boffperf_L1J15'
    dh = dict(help='(default: %(default)s)')
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('input_files', nargs='+')
    parser.add_argument('-m','--max-events', type=int, nargs='?', const=10)
    parser.add_argument('-t','--threads', type=int, default=0)
    parser.add_argument('-d','--debug', action='store_true')
    parser.add_argument('-n','--chain', default=default_chain, **dh)
    parser.add_argument('-c','--config-file', required=True)
    parser.add_argument('-o','--output', default='output.h5', **dh)
    parser.add_argument('-i','--event-print-interval', type=int,
                        default=100, **dh)
    return parser.parse_args()

def trigDatasetDumper(cfgFlags, chain, config_file, output_file):
    ca = ComponentAccumulator()

    # build labeling collections
    labelAlg = trig.getLabelingBuilderAlg(cfgFlags)
    ca.addEventAlgo(labelAlg)

    # get jets
    temp_btag, temp_jets = 'tempBtag', 'tempJets'
    ca.merge(trig.triggerJetGetterCfg(
        cfgFlags,
        chain=chain,
        temp_jets=temp_jets,
        temp_btag=temp_btag,
    ))

    # match to offline jets, pull out some info
    matcher = CompFactory.TriggerBTagMatcherAlg('matcher')
    matcher.offlineBtagKey = 'BTagging_AntiKt4EMPFlow'
    matcher.triggerBtagKey = temp_btag
    taggersToCopy = ['DL1r','DL1dv00']
    matcher.floatsToCopy = {
        f'{tagger}_p{flav}':f'OfflineMatched{tagger}_p{flav}' for tagger in taggersToCopy for flav in 'bcu'
    }
    matcher.offlineJetKey = 'AntiKt4EMPFlowJets'
    matcher.triggerJetKey = temp_jets
    truth_labels = [
        'HadronConeExclTruthLabelID',
        'HadronConeExclExtendedTruthLabelID',
    ]
    matcher.jetIntsToCopy = {
        x:f'OfflineMatched{x}' for x in truth_labels
    }
    ca.addEventAlgo(matcher)

    # associate fullscan tracks to the jets
    fs_tracks = 'HLT_IDTrack_FS_FTF'
    fs_vertices = 'HLT_IDVertex_FS'
    ca.merge(trig.getTrackAugmentation(
        cfgFlags,
        tpc=fs_tracks,
        pvc=fs_vertices,
    ))
    ca.addEventAlgo(CompFactory.FlavorTagDiscriminants.PoorMansIpAugmenterAlg(
        trackContainer=fs_tracks,
        primaryVertexContainer=fs_vertices
    ))
    ca.merge(trig.getFixedConeTrackAssociationAlgs(cfgFlags, temp_jets, temp_btag))
    ca.merge(dumper.getDumperConfig(config_file, output_file))

    return ca


def run():
    args = get_args()

    from AthenaConfiguration.AllConfigFlags import ConfigFlags as cfgFlags

    cfgFlags.Input.Files = list(
        chain.from_iterable(f.split(',') for f in args.input_files))

    if args.max_events:
        cfgFlags.Exec.MaxEvents = args.max_events

    cfgFlags.Concurrency.NumThreads = args.threads
    if args.threads:
        cfgFlags.Concurrency.NumConcurrentEvents = args.threads
    cfgFlags.Exec.OutputLevel = INFO
    if args.debug:
        cfgFlags.Exec.OutputLevel = DEBUG
        cfgFlags.Exec.DebugStage = 'exec'

    cfgFlags.lock()

    #########################################################################
    ################### Build the component accumulator #####################
    #########################################################################
    #
    ca = getConfig(cfgFlags)
    ca.addService(CompFactory.AthenaEventLoopMgr(
        EventPrintoutInterval=args.event_print_interval))

    # This is also needed for TDT
    ca.merge(MetaDataSvcCfg(cfgFlags))

    # Needed to read anything from a file
    ca.merge(PoolReadCfg(cfgFlags))

    ca.merge(
        trigDatasetDumper(
            cfgFlags,
            chain=args.chain,
            config_file=args.config_file,
            output_file=args.output,
        )
    )


    #########################################################################
    ########################### Run everything ##############################
    #########################################################################
    return ca.run()

if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
