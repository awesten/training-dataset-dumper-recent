#ifndef HBB_OPTIONS_HH
#define HBB_OPTIONS_HH

#include <vector>
#include <string>

struct IOOpts
{
  std::vector<std::string> in;
  std::string config_file_name;
  std::string out;
  size_t n_entries;
  bool verbose;
  bool ignore_xrootd_fail;
};

IOOpts get_io_opts(int argc, char* argv[]);


#endif
