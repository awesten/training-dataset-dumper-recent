
"""
Dumper interface

"""

import argparse
import os

from GaudiKernel.Configurable import DEBUG, INFO

from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator


class DumperHelpFormatter(
    argparse.RawTextHelpFormatter, argparse.ArgumentDefaultsHelpFormatter
):
    ...


def base_parser(description, add_help=True):
    parser = argparse.ArgumentParser(description=description, formatter_class=DumperHelpFormatter, add_help=add_help)
    parser.add_argument("input_files", nargs="+", help="comma or space separated list of input filenames")
    parser.add_argument("-o", "--output", default="output.h5", help="output filename")
    parser.add_argument("-c", "--config-file", required=True, help="job configuration file")
    parser.add_argument("-m", "--max-events", type=int, nargs="?", help="number of events to process")
    parser.add_argument("-i", "--event-print-interval", type=int, default=100, help="set output frequency")
    parser.add_argument("-d", "--debug", action="store_true", help="set output level to DEBUG")
    parser.add_argument("-g", "--debugger", action="store_true", help="attach debugger at execute step")
    return parser


def update_cfgFlags(cfgFlags, args):

    # parse input files
    if len(args.input_files) > 1:
        if any("," in f for f in args.input_files):
            raise ValueError(
                "you provided a mix of spaces and commas"
                " in your input file list, this is probably an error"
            )
        cfgFlags.Input.Files = args.input_files
    else:
        cfgFlags.Input.Files = args.input_files[0].rstrip(",").split(",")

    # set number of events to process
    if args.max_events:
        cfgFlags.Exec.MaxEvents = args.max_events

    # set output level
    if args.debug:
        cfgFlags.Exec.OutputLevel = DEBUG

    # attach debugger
    if args.debugger:
        prepGdb()
        cfgFlags.Exec.DebugStage = "exec"

    return cfgFlags


def prepGdb():
    """
    Running gdb is sometimes more difficult than it should be, this
    runs some workarounds for the debugger in images.
    """
    # The environment variable I'm checking here just happens to be
    # set to a directory you would not have access to outside images.
    if os.environ.get('LCG_RELEASE_BASE') == '/opt/lcg':
        del os.environ['PYTHONHOME']


def getDumperConfig(config, output_path):
    ca = ComponentAccumulator()

    output = CompFactory.H5FileSvc(path=output_path)
    ca.addService(output)

    btagAlg = CompFactory.SingleBTagAlg('DatasetDumper')
    btagAlg.output = output
    btagAlg.configFileName = config
    ca.addEventAlgo(btagAlg)

    return ca
